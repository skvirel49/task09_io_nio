package com.bilas.serialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Ship implements Serializable {
    private String name;
    private transient int speed;
    private List<Droid> droidList;

    public Ship(String name, int speed) {
        this.name = name;
        this.speed = speed;
        this.droidList = new ArrayList<>();
        addDroids();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public List<Droid> getDroidList() {
        return droidList;
    }

    public void setDroidList(List<Droid> droidList) {
        this.droidList = droidList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return speed == ship.speed &&
                Objects.equals(name, ship.name) &&
                Objects.equals(droidList, ship.droidList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, speed, droidList);
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", speed=" + speed +
                ", droidList=" + droidList +
                '}';
    }

    private void addDroids(){
        int ran = (int) (Math.random() * 10) +1;
        for (int i = 0; i < ran; i++) {
            droidList.add(new Droid(ran + ran * (ran / 2)));
        }
    }
}
