package com.bilas.serialize;

import com.bilas.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Serialize {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static final String SERIALIZE_FILE_NAME = "droids.dat";
    private Ship ship;

    public Serialize() throws ClassNotFoundException, IOException {
        this.ship = new Ship("Pobeda", 45);
    }

    public void serializeShip(Ship ship, String fileName){
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName))) {
            objectOutputStream.writeObject(ship);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Ship deserializeShip() {
        File file = new File(SERIALIZE_FILE_NAME);
        Ship readShip = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            readShip = (Ship) objectInputStream.readObject();
            LOGGER.info(readShip);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return readShip;
    }
}
