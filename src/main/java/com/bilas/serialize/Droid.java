package com.bilas.serialize;

import java.io.Serializable;
import java.util.Objects;

public class Droid implements Serializable {
    private static int idCount = 1;
    private int id;
    private int damage;
    private transient int life;

    public Droid(int damage) {
        this.id = idCount++;
        this.damage = damage;
        this.life = 100;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Droid droid = (Droid) o;
        return id == droid.id &&
                damage == droid.damage &&
                life == droid.life;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, damage, life);
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id=" + id +
                ", damage=" + damage +
                ", life=" + life +
                '}';
    }
}
