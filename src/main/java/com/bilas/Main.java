package com.bilas;

import com.bilas.serialize.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Main {
    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    public static void main(String[] args) {

        Ship ship = new Ship("Pobeda", 20);

        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("droids.dat"))) {
            objectOutputStream.writeObject(ship);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file = new File("droids.dat");
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            Ship readShip = (Ship) objectInputStream.readObject();
            LOGGER.info(readShip);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
